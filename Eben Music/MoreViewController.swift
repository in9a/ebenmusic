//
//  MoreViewController.swift
//  Eben Music
//
//  Created by 1 on 1/3/17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import UIKit

class MoreViewController: BaseViewController {
    
    @IBOutlet weak var moreTableViewOutlet: UITableView!
    
    let moreArray = [
        ["image":"news","title":"News"],
        ["image":"weather","title":"Weather"],
        ["image":"horoscope","title":"Horoscope"],
        ["image":"feedback","title":"Leave Feedback/Rate App"],
        ["image":"about","title":"About"]]
    
    override func setup() {
        
        titleForNavigation = "MORE"
        moreTableViewOutlet.reloadData()
        moreTableViewOutlet.tableFooterView = UIView()
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem 
    }
    
}

extension MoreViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 56
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){

        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "newsSegue", sender: self)
        case 1:break
        case 2:break
        case 3:break
        case 4:break
            
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return moreArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = moreTableViewOutlet.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MoreTableViewCell
        
        cell.imageViewOutlet.image = UIImage(named: moreArray[indexPath.row]["image"]!)
        cell.titleLabelOutlet.text = moreArray[indexPath.row]["title"]
        
        return cell
    }
    
}

