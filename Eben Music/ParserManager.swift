//
//  ParserManager.swift
//  ParserTest
//
//  Created by Absolute iOS 2 on 05.01.17.
//  Copyright © 2017 Absolute Web Services. All rights reserved.
//

import Foundation
protocol ParserManagerDelegate:class {
    func reciveAllItems(_ ebenMusicItems:[EbenMusicItem])
}

class ParserManager:NSObject {
    
    public func parseItem (_ data:Data) {
        let parser = XMLParser(data: data)
        parser.delegate = self
        parser.parse()
    }
    
    fileprivate var currentElement:String?
    fileprivate var itemsArray = [EbenMusicItem]()
    fileprivate var item:EbenMusicItem?
    fileprivate var foundedString:String = ""
    
    var delegate:ParserManagerDelegate
    
    init(withDelegate delegate:ParserManagerDelegate) {
        self.delegate = delegate
    }
}

extension ParserManager: XMLParserDelegate {
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print(parseError)
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        
        
        if elementName == "item" {
            item = EbenMusicItem(title: nil, link: nil, description: nil, date: nil)
        }
        
        currentElement = elementName
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if foundedString.characters.count > 0 {
            if elementName == "title" {
                item?.title = foundedString
            }
            else if elementName == "link" {
                if let link = URL(string: foundedString) {
                    item?.link = link
                }
            }
                
            else if elementName == "description" {
                item?.description = foundedString
            }
                
            else if elementName == "pubDate" {
                item?.date = foundedString
            }
            
            
        }
        if elementName == "item" {
            if let finalItem = item {
                itemsArray.append(finalItem)
            }
        }
        foundedString = ""
        
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if currentElement == "title" || currentElement == "link" || currentElement == "description" || currentElement == "pubDate" {
            if !string.contains("\n") {
                foundedString.append(string)
            }
        }
        
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        print("OKE OK")
        
        if itemsArray.count > 0 {
            
            self.delegate.reciveAllItems(self.itemsArray)
            
        }
        
        
        for obj in itemsArray {
            print(obj.title ?? "4et ne to")
            print(obj.link ?? "4et ne to")
            print(obj.description ?? "4et ne to")
        }
    }
}
 
