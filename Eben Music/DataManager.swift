//
//  DataManager.swift
//  Eben Music
//
//  Created by 1 on 1/5/17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import Foundation

protocol DataManagerDelegate {
    func reciveNewsItems(_ ebenMusicItems:[EbenMusicItem])
    func toggleError(_ error:Error)
}

class DataManager {
    private var serverManager = ServerManager()
    
    private lazy var parser:ParserManager = {
        return ParserManager(withDelegate: self)
    }()
    
    var delegate: DataManagerDelegate
    
    init (withDelegate delegate:DataManagerDelegate){
        self.delegate = delegate
    }
    
    public func getNews() {
        serverManager.getNews({ (xmlData) in
            self.parser.parseItem(xmlData)
        }) { (error) in
            self.delegate.toggleError(error)
        }
    }
}

extension DataManager : ParserManagerDelegate {
    func reciveAllItems(_ ebenMusicItems:[EbenMusicItem]) {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else {return}
            
            strongSelf.delegate.reciveNewsItems(ebenMusicItems)
        }
        
    }
}
