//
//  ServerManager.swift
//  Eben Music
//
//  Created by 1 on 1/4/17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import Foundation
import Alamofire

class ServerManager{
    
    func getNews(_ onSuccess: @escaping (Data) -> (),_ onError: @escaping (Error) -> () ) {
        
        Alamofire.request("http://dev.ebenmusic.com/api/rss/rss_en.xml", method: .get).responseData(completionHandler: { (response) in
            
            if let xmlData = response.result.value {
                    onSuccess(xmlData)
            }else{
                onError(response.result.error ?? CustomError.customError)
            }
        
        })

    }
    
}

enum CustomError:Error {
    case customError
}
