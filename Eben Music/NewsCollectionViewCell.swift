//
//  NewsCollectionViewCell.swift
//  Eben Music
//
//  Created by 1 on 1/4/17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewOutlet: UIImageView!
    @IBOutlet weak var titleLabelOutlet: UILabel!
    @IBOutlet weak var dateLabelOutlet: UILabel!
    
    func setupNewsCell(withItem item:EbenMusicItem){
        
        self.titleLabelOutlet.text = item.title
        
        
        
        self.dateLabelOutlet.text = item.dateFromMusicString()
        
        
    }
    
    
}
