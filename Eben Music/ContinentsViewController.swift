//
//  MusicViewController.swift
//  Eben Music
//
//  Created by 1 on 1/3/17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import UIKit

class ContinentsViewController: BaseViewController {
    
    
    @IBOutlet weak var musicTableViewOutlet: UITableView!
    
    let continentsArray = ["AFRICA","AMERICA","ASIA","EUROPE","OCEANIA"]
    
    override func setup() {
        
        titleImageLogo = "eben_logo"
        musicTableViewOutlet.reloadData()
        
        musicTableViewOutlet.tableFooterView = UIView()
    }



}

extension ContinentsViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return continentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = musicTableViewOutlet.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ContinentTableViewCell
        
        cell.titleLabelOutlet?.text = continentsArray[indexPath.row]
        
        return cell
    }
    
    
}
