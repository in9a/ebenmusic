//
//  EMNewsItem.swift
//  Eben Music
//
//  Created by Absolute iOS 2 on 10.01.17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import Foundation

struct EbenMusicItem { 
    var title:String?
    var link:URL?
    var description:String?
    var date:String?
    
    func dateFromMusicString () -> String {
        
        let df = DateFormatter()
        df.locale = Locale(identifier: "en_US_POSIX")
        df.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
        
        if let currentDateString = self.date, let date = df.date(from: currentDateString) {
            
            df.dateFormat = "dd MMM yyyy"
            
            return df.string(from: date)
        }
        
        return ""
    }
}
