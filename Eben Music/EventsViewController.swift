//
//  EventsViewController.swift
//  Eben Music
//
//  Created by 1 on 1/3/17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import UIKit

class EventsViewController: BaseViewController {
    
    
    @IBOutlet weak var eventsTableViewOutlet: UITableView!
    
    
    override func setup() {
        
        titleForNavigation = "EVENTS"
        eventsTableViewOutlet.reloadData()
        
    }
    
    
}

extension EventsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80
    }
    
}

extension EventsViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = eventsTableViewOutlet.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! EventsTableViewCell
        
        
        return cell
    }
    
    
}
