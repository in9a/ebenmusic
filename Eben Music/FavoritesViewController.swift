//
//  FavoritesViewController.swift
//  Eben Music
//
//  Created by 1 on 1/3/17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import UIKit

class FavoritesViewController: BaseViewController {
    
    
    @IBOutlet weak var favoritesTableViewOutlet: UITableView!
    
    
    

    override func setup() {
        titleForNavigation = "FAVORITES"
        
        favoritesTableViewOutlet.reloadData()
    }
    
    
    
    

}

extension FavoritesViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = favoritesTableViewOutlet.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FavoritesTableViewCell
        
        cell.songTitleLabelOutlet.text = "Kayna Samet"
        cell.countryLabelOutlet.text = "Algeria, Soul"
        
        return cell
    }
}

